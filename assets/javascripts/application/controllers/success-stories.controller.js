/* Prepare local variables that will be reused in different methods */
let $successStoriesContainer, _successStoriesNextItemTemplate

$(function () {
  // Prepare coming next containers
  $successStoriesContainer = $('.js-success-stories-list')
  _successStoriesNextItemTemplate = $('.js-success-stories-item').clone(true)

  $('.js-success-stories-item').remove()

  // Initiate the full collection of coming-next collection
  if ($('.js-successStoriesController').length) {
    successStory()
  }
})

const successStory = () => {
  getSuccessStories(_data => { // eslint-disable-line no-undef
    const successStoryData = _data.data.successes

    for (let i = 0; i < successStoryData.length; i++) {
      const newsuccessStoryItem = _successStoriesNextItemTemplate.clone(true)

      // Add design specs info
      $(newsuccessStoryItem).find('.js-success-stories-title').text(successStoryData[i].title)
      $(newsuccessStoryItem).find('.js-success-stories-description').text(successStoryData[i].description)
      $(newsuccessStoryItem).find('.js-success-stories-image').attr('src', successStoryData[i].image)
      $($successStoriesContainer).append(newsuccessStoryItem)
    }
  })
}
