const getComponentService = (callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/javascripts/application/models/component-showcase.json`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the component showcase data`)
        callback()
      }
    }))
    .then(function (data) {
      callback(data)
    })
}
