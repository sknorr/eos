const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('writing/conventions-and-rules')
})

module.exports = router
